(function()
{
	angular
		.module('app')
		.controller('appController',appController);

	function appController($scope,$window)
	{
		$scope.leftPanelWidth = ($window.innerWidth * 0.15);
		$scope.mainContentWidth = ($window.innerWidth * 0.80);
		$scope.contentWidth = $window.innerWidth;
	}
})();