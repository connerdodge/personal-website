(function()
{
	angular
		.module('app')
		.controller('clockController',clockController);

	function clockController($scope, $interval, $window) 
	{
	  	$scope.theTime = new Date().toLocaleTimeString();
		$interval(function () 
		{
		    $scope.theTime = new Date().toLocaleTimeString();
		}, 1000);
	}
})();
