(function()
{
	angular
		.module('app')
		.controller('converterController',converterController);

	function converterController($scope) 
	{
		$scope.vals = [{},{},{}];

		$scope.$watch('hexVal',function(newVal)
		{
			if(!newVal)
			{
				$scope.vals[0].bin = 0;
				$scope.vals[0].dec = 0;
				return;
			}
			$scope.vals[0].bin = parseInt(newVal,16).toString(2);
			$scope.vals[0].dec = parseInt(newVal,16).toString(10);
		});

		$scope.$watch('binaryVal',function(newVal)
		{
			if(!newVal)
			{
				$scope.vals[1].dec = 0;
				$scope.vals[1].hex = 0;
				return;
			}
			$scope.vals[1].dec = parseInt(newVal,2).toString(10);
			$scope.vals[1].hex = parseInt(newVal,2).toString(16);
		});

		$scope.$watch('decimalVal',function(newVal)
		{
			if(!newVal)
			{
				$scope.vals[2].bin = 0;
				$scope.vals[2].hex = 0;
				return;
			}
			$scope.vals[2].bin = parseInt(newVal,10).toString(2);
			$scope.vals[2].hex = parseInt(newVal,10).toString(16);
		});
	}
})();

